# Intro
OMC-bot is a Node JS replacement for lil-bot. A bot to help automate moderation activities on our new platform, Matrix.

# Resources
* matrix-sdk-github
    * https://github.com/matrix-org/matrix-js-sdk
* matrix-js-sdk reference
    * http://matrix-org.github.io/matrix-js-sdk/stable
* sequelize documentation
    * https://sequelize.org/docs/v6/

TODO: add resource links

# TODO
Planned new features (out of order)

- [ ] Create md documentation files
    - [ ] Explain main.js
    - [ ] Introduce matrix-js-sdk
    - [ ] Introduce sequelize
- [x] Create config files
    - [x] Move logins to an encrypted file
    - [x] Make config file just for default settings, and put current settings in the DB
    - [ ] Config file setting to override DB or even to start clean
- [ ] Create a command-line interface
- [ ] Create a command listener (Matrix-side)
- [ ] Commands:
    - [ ] Command to change bot's global display name
    - [ ] Command to change bot's global pfp (provide url or message containing an image)
    - [ ] Help command, obviously
    - [ ] Hug command!
- [ ] Set power level upon invite as per original room
- [ ] SQL database (with sequelize)
    - [ ] Log already invited users
    - [ ] Log invitation status (error, accepted, rejected, pending)
    - [ ] Allow clearing old db records
    - [ ] Remember mods. Commands to add, remove, change.

# Developing
# Running
The file `logins.bot.json` is encrypted with git-crypt. There is an article on how to use it below. Access is not yet set up for anyone but butterfly.omc, so please ask her for the login if you want to run the bot.

If you want to run the bot, you must install these dependencies:

* node-js
    * This will differ from OS to OS
* git-crypt
    * https://dzone.com/articles/managing-secrets-with-git-crypt
* matrix-js-sdk, the matrix JS library
    * `yarn add matrix-js-sdk`
* sequelize & sqlite3 (for databases)
    * `npm install sequelize sqlite3`

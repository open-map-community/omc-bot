const initialData = {
	positions: {
		courtyard: "",
		onboarded: "",
		member: "",
		dm: "",
		associateModerator: "",
		admin: ""
	},
	rooms: {
		omc: {
			identifier: "#omc:crypted.cafe",
			type: "space"
		},
		mapped: {
			identifier: "#omc-mapped:crypted.cafe",
			type: "space"
		},
		onboarding: {
			identifier: "#omc-onboarding:crypted.cafe",
			type: "room"
		},
		welcome: {
			identifier: "#omc-welcome:crypted.cafe",
			type: "room"
		},
		general: {
			identifier: "!ZzzLZmwGKvgAFPLzYx:crypted.cafe",
			type: "room"
		},
		cuteGirls: {
			identifier: "!SJlAamwPJMsqKrZFTl:crypted.cafe",
			type: "room"
		},
		food: {
			identifier: "#omc-food:crypted.cafe",
			type: "room"
		},
		news: {
			identifier: "#omc-news:crypted.cafe",
			type: "room"
		},
		spirituality: {
			identifier: "#omc-spirituality:crypted.cafe",
			type: "room"
		},
		spam: {
			identifier: "#omc-spam:crypted.cafe",
			type: "room"
		},
		miscellaneous: {
			identifier: "#omc-miscellaneous:crypted.cafe",
			type: "room"
		},
		stem: {
			identifier: "#omc-stem:crypted.cafe",
			type: "room"
		},
		development: {
			identifier: "#omc-development:crypted.cafe",
			type: "room"
		},
		artsAndMusic: {
			identifier: "#omc-arts:crypted.cafe",
			type: "room"
		}
	}
}

module.exports = { initialData };

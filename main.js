// Import the olm (encryption) library
// global.Olm = require('olm'); // Must come before loading matrix sdk. This is the encryption library
const sequelize = require('sequelize'); // sequelize is an SQL library. We'll use it for our bot's databases (DBs)
const sdk = require("matrix-js-sdk"); // The matrix library
const config = require("./config.js");

// --------------- Database Initialization ---------------
// Each room group (such as MAP rooms or Onboarding rooms) should
// have its own table with rows corresponding to IDs of rooms in
// that group

// The bot should probably just grab room names from Matrix, and
// use room ID's only in the code

const sequelizeDatabase = new sequelize.Sequelize({
	dialect: 'sqlite',
	storage: 'data/database.sqlite',
	define: {
		freezeTableName: true
	}
});

try {
	sequelizeDatabase.authenticate();
	console.log('Connection has been established successfully.');
} catch (error) {
	console.error('Unable to connect to the database:', error);
}

// const tables =*/ {};

/*tables.rooms =*/ sequelizeDatabase.define('rooms', {
	roomId: {
		type: sequelize.DataTypes.STRING,
		unique: true,
		allowNull: false
	},
	alias: sequelize.DataTypes.STRING,
	type: sequelize.DataTypes.STRING
	// refer to rooms via their alias
});

/*tables.persons =*/ sequelizeDatabase.define('persons', {
	personId: {
		type: sequelize.DataTypes.STRING,
		unique: true,
		allowNull: false
	},
	name: sequelize.DataTypes.STRING,
	positionId: sequelize.DataTypes.STRING // Onboarded, mapped, DM'ed, Moderator, Associate Moderator, etc
});

/*tables.accounts =*/ sequelizeDatabase.define('accounts', {
	accountId: {
		type: sequelize.DataTypes.STRING,
		unique: true,
		allowNull: false
	},
	personId: sequelize.DataTypes.STRING
});

/*tables.positions =*/ sequelizeDatabase.define('positions', {
	positionId: {
		type: sequelize.DataTypes.STRING,
		unique: true,
		allowNull: false
	},
	powerLevel: sequelize.DataTypes.INTEGER,
});

// tables.bans

// Sync the databases
// TODO: force reset/wipe the databases if the proper command line
// argument to do so is set
// for(model of sequelizeDatabase.models) {
	// model.sync();
// }


// --------------- Config ---------------
const testroomID = "!LfQhMQQImWdyjhiYYi:matrix.org"; // butterfly4's private room
const botPrefix = "OMC Bot:"; // Prefix used when sending messages

// ---------------Logins---------------
// Files named logins.<text>.json store logins for different accounts. You can use them
// to run this code as any matrix user, including yourself. To use your own login, create
// a file with the login, put it in userLogin, then change clientLogin to use userLogin
// instead of botLogin.
const botLogin = require('./logins.bot.json'); // Bot credentials file
const userLogin = require('./logins.butterfly.json'); // Credentials for another account
const clientLogin = botLogin;

// ---------------Data ---------------
// This will eventually be moved to the database so it can be changed using commands.


// These are room IDs. Rooms in Matrix have unique IDs that don't change but their public
// alias (e.g. #cute-girls:crypted.cafe)  can change. The backend usually deals with IDs
// and not aliases

// List of users who consistently fail to be invited due to server issues.
// They are ignored to speed up running of the bot. These will be stored in
// the database and retried periodically.
var errorUsers = [ '@bananacandy:cutefunny.art' ];


// --------------- Initialize Client ---------------

// Initialize the matrix client code object.
clientLogin.baseUrl = 'https://matrix.org';
clientLogin.deviceId = 'js-bot';
const client = sdk.createClient(clientLogin);


// --------------- Define Functions  ---------------

// This function will handle errors from common matrix operations like inviting members to rooms, joining rooms, etc
// It should have a different return value depending on if the issue is an HTTPError, an M_FORBIDDEN Matrix error, or
// simply a ratelimit. And of course for unrecognized errors as well.
// In this context, there's 3 types of errors:
//	Unrecoverable errors. The operation simply failed
//	Timeouts
//	Unknown errors
// Returns an array:
//		["unrecoverable", "M_FORBIDDEN", errorObject];
//		["timeout", "M_LIMIT_EXCEEDED", errorObject, timeoutPromise];
//		["unrecognizedMatrixError", errorObject.name, errorObject];
//		["unrecoverable", "HTTPError", errorObject];
//		["unrecognized", errorObject.name, errorObject];
async function matrixErrorHandeler(errorObject, operationDetails) {
	// TODO: move console.log calls to the functions that handle the return value of this function? I don't think so tbh because we should have all errors log. There is no context where an error should not be logged. Unless debugging is turned off, perhaps, which is a more advanced feature
	// TODO: user operationDetails in console.log
	if(errorObject instanceof sdk.MatrixError) { // If the error is a Matrix error
		if(errorObject.name === "M_FORBIDDEN") { // Likely a permission error
			console.log(`${operationDetails} failed, error:`, errorObject.data.error); // Print the error and continue
			// TODO: If M_FORBIDDEN is necessarily a permission error, then say so in console.log and log it either in the bot log channel/thread or in the logfiles/databases (or both).
			// continue;
			return ["unrecoverable", "M_FORBIDDEN", errorObject];
		}
		// if it's a timeout, wait the timeout period, decrement i to repeat the cycle for this user, break.
		else if(errorObject.name === "M_LIMIT_EXCEEDED" && errorObject.data.error === "Too Many Requests") {
			console.log(`${operationDetails} reached timeout of ${errorObject.data.retry_after_ms} ms`);
			const timeoutPromise = new Promise(r => setTimeout(r, errorObject.data.retry_after_ms)); // create a promise that will resolve when the timeout is up
			return ["timeout", "M_LIMIT_EXCEEDED", errorObject, timeoutPromise];
		}
		else { // some other matrix error
			console.log(`${operationDetails} failed. Error:`, errorObject);
			return ["unrecognizedMatrixError", errorObject.name, errorObject];
		}
	}
	else if(errorObject instanceof sdk.HTTPError) { // If it's an HTTPError (thus likely a server error)
		console.log(`${operationDetails} failed with HTTPError. Error:`, errorObject);
		return ["unrecoverable", "HTTPError", errorObject];

	}
	else { // For all other errors (likely a code error) stop the code and print the error
		console.log(`${operationDetails} failed with an unrecognized error. Error:`, errorObject);
		return ["unrecognized", errorObject.name, errorObject];
		// throw errorObject;
	}
}

// This function will invite all members of one room to another room
// example:
// await inviteMembersInRoom(rooms.general, rooms.cuteGirls, "new room");
async function inviteMembersInRoom(baseRoomId, inviteRoomId, reason) { 
 	const members = await client.getJoinedRoomMembers(baseRoomId); // Returns IJoinedMembersResponse.
	
	// console.log('members:', members);
	const joinedMemberIds = Object.keys(members.joined); // from the IJoinedMembersResponse grab the list of joined userIDs (.members.joined.*). This is an array of IDs
	const inviteRoom = client.getRoom(inviteRoomId); // Get the room object for the invite room
	if(!inviteRoom) { // If the room can't be found by its ID
		console.log(`inviteMembersInRoom: Can't find room ${inviteRoomId}. client.getRoom() returns:`, inviteRoom);
		console.log(`Check the bot is actually in this room`)
		return;
	}
	if(inviteRoom.canInvite(client.userId) === false) { // check if the client has permissions to invite users to this room
		console.log(`Client doesn't have permission to invite users to ${inviteRoom.getDefaultRoomName(client.userId)} ${inviteRoomId}`);
		return;
	}
	const inviteRoomMembers = inviteRoom.getMembers(); // get a list of people already in the invite room as not to invite them again

	// console.log('inviteRoomMembers:', inviteRoomMembers);

	for(let  i = 0; i < joinedMemberIds.length; i++) { // Loop over the array of users in the base room to invite each one
		const memberId = joinedMemberIds[i]; // Id of current member of base room
		// TODO: promote mods & admins
		if(inviteRoomMembers.filter((x) => x.userId === memberId).length > 0) { // If ID of current member of baseroom is also in the inviteRoomMembers array, then don't invite. TODO: this is a double loop, reduce redundancy if possible.
			console.log(`${memberId} is already invited to this room or a member of it`);
			// TODO: check perms
			// TODO: promote()
			continue; // skip the rest of the code in this loop since they don't need to be invited
		}
		else if(errorUsers.indexOf(memberId) !== -1) { // If the ID of the current user is in the errorUsers array, also skip them
			console.log(`Ignoring error user ${memberId}`);
			continue;
		} // otherwise, they should be invited:
		console.log(`Trying to invite ${memberId}`)
		const inviteDetails = `inviting ${memberId} to ${inviteRoom.getDefaultRoomName(client.userId)}`; // this string is used for logging messages. Lets us know what is trying to be done when an error occurs
		try {
			if(!reason) // if the reason is blank, don't use it
				// TODO: introduce asynchronicity. Don't await calls unless the list of pending invites goes below five
				await client.invite(inviteRoomId, memberId);
			else // otherwise use it
				await client.invite(inviteRoomId, memberId, reason);
			// TODO: check success
			// TODO: promote()
			// TODO: check their membership in baseRoomId, mod them.
		} catch(e) { // If the invite fails
			const handelerResponse = await matrixErrorHandeler(e, inviteDetails);
			if(handelerResponse[0] === "timeout") {
				console.log(`Timeout reached, waiting ${e.data.retry_after_ms} ms`); // wait the timeout
				await handelerResponse[3];
				i--;
				continue;
			} else if(handelerResponse[0] === "unrecoverable") {
				// TODO: log the issue in the DB
				// if(handelerResponse[1] === "HTTPError")
					// TODO: add user to errorUsers.
				continue;
			}
		}
	}
	console.log("Invite function complete");
}

// This function will check rooms a client is a member of and find ones where the membership
// Status is 'invite' (this means the client is invited to the room but hasn't joined) and join them
async function joinInvitedRooms() {
	const context = 'joinInvitedRooms()';
	const joinedRooms = client.getRooms();
	for(const room of joinedRooms) {
		// TODO selfMembership === invite, then join
		if(room.selfMembership === 'invite') {
			console.log(`Joining invited room ${room.roomId}`);
			try {
				await client.joinRoom(room.roomId);
			} catch (e) { // TODO: create a more advanced error handler. Timeouts, matrix errors, http errors, etc.
				// console.log(`Error joining room ${room.roomId}. Error:`, e);
				console.log(`Error joining room ${room}. Error:`, e);
			}
		}
		else {
			console.log(`${context}: Already in ${room.roomId}. Membership: ${room.selfMembership}`);
		}
	}
}


async function populateDatabasesFromClient(databases) {
	const context = 'populateDatabasesFromClient';
	console.log(`Entered ${context}`);
	for(const database of databases) {
		if(database === 'rooms') {
			// console.log(`${context}: b4 4 loop`);
			const rooms = client.getRooms();
			// console.log(`${context} rooms:`, rooms);
			for(const room of rooms) {
				// console.log(`${context}: room ${room.name}`);
				// TODO: replace room.type
				record = sequelizeDatabase.models[database].build({ type: room.type });

				record.roomId = room.roomId;
				const matchingRooms = await sequelizeDatabase.models[database].findAndCountAll({ where: { roomId: record.roomId } });

				// Get the room alias
				aliasResult = room.getCanonicalAlias();
				if(!aliasResult)
					continue;

				// Check if the room is in DB and filled out
				if(matchingRooms.count > 0) {
					console.log(`${context}: matchingRooms.rows[0].alias:`, matchingRooms.rows[0].alias);
					if(matchingRooms.rows[0].getDataValue('alias') === aliasResult) {
						// if this room is already in the db and has values filled out, skip
						console.log(`${context}: Room ${room.name} is in DB & has all fields filled, skipping`);
						continue;
					} else {
						// If it doesn't have aliases, use the DB record and fill its alias
						record = matchingRooms.rows[0];
						console.log(`${context}: Room ${room.name} is in DB but is incomplete, adding`);
					}
				}
				record.alias = aliasResult;

				try {
					await record.save().then(() => console.log(`${context}: Successfully inserted ${room.name} into ${data[database]}`));
				} catch(e) {
					if(e){}
					console.log(`${context}: Error inserting record into database. Record, error:`, record, e);
					// catch
				}
			}
		}
	}
}


// This function will populate the databases with
// the data provided to it as an object
// Note: databases array items must have the same name
// as keys in the data object AND actual database names
// example:
// await populateDatabases(initialData, ['rooms'])
async function populateDatabases(data, databases) {
	for(const database of databases) {
		if(database === 'rooms') {
			// requires that the db's have the same name as the ones in the argument
			// console.log('data[database] (data.rooms):', data[database]);
			for(const roomName in data[database]) {
				const room = data[database][roomName];
				record = sequelizeDatabase.models[database].build({ type: room.type });
				if(/^!/.test(room.identifier)) { // If the identifier starts with a ! (aka it's an id)
					record.roomId = room.identifier;
					// if this room is already in the db, skip
					const matchingRooms = await sequelizeDatabase.models[database].findAndCountAll({ where: { roomId: record.roomId } });
					if(matchingRooms.count > 0) {
						console.log('matchingRooms.rows[0].alias:', matchingRooms.rows[0].alias);
						if(matchingRooms.rows[0].getDataValue('alias')) {
							continue;
						} else {
							// use the result as the record
							record = matchingRooms.rows[0];
						}
					}
					aliasResult = await client.getLocalAliases(room.identifier).then((successResult, failureResult) => { 
						if(failureResult) {
							console.log('Error fetching room aliases:', failureResult);
						}
						return successResult[0]
					}); // get room alias(es)
					console.log('aliasResult:', aliasResult);
					record.alias = aliasResult;
				}
				else if(/^#/.test(room.identifier)) { // If the identifier starts with a # (aka it's an alias)
					rraObj = await client.resolveRoomAlias(room.identifier);
					record.roomId = rraObj.room_id;
					// if this room is already in the db, skip
					if(await sequelizeDatabase.models[database].findAndCountAll({ where: { roomId: record.roomId } }).then((result) => { return result.count }) > 0) {
						continue;
					}
					record.alias = room.identifier;
				}
				else {
					console.log(`Room identifier '${room.identifier}' for room '${roomName}' not recognized`);
					continue; // Skip inserting this record
				}
				try {
					// TODO: check if an update is needed instead. Or look at what model.create entails and break it apart
					// await sequelizeDatabase.models[database].create(record).then(() => console.log(`Successfully inserted ${roomName} into ${data[database]}`));
					// const newRecord = 
					await record.save().then(() => console.log(`Successfully inserted ${roomName} into ${data[database]}`));
				} catch(e) {
					if(e){}
					console.log(`populateDatabases: Error inserting record into database. Record, error:`, record, e);
					// catch
				}
			}
		}
		else if(database === '') {
		}
	}
}

// This function sets the power level of one or more users to the
// same power level in the target room as they have in the base room
// if they don't already have the same or a higher power level.
async function opToBaseRoomPermissions(targetRoom, targetUserArray) {
	if(targetUserArray[0] === "all") { // if what is desired is OP'ing all members
		// loop through members in the base room
		// find their power level
		// theck their membership in the target room
		// check their power level
		// if it's lower than the base room, OP them.
		// bot can only OP to the same level it has
	}
}

// --------------- Main Code  ---------------
// TODO: Catch invite events and join invited room
client.once("sync", async function (state, prevState, res) { // everything here will run once the client sync is complete
	if (state === "PREPARED") { // This will run if the sync completed with success
		console.log("client.once('sync'): state is prepared");

		// Main code runs here
		// TODO: set up a command listener

		const popProm = populateDatabasesFromClient(['rooms']);
		const joinProm = joinInvitedRooms(); // Join all rooms the bot is invited to but isn't a member of

		// const inviteRoom = await client.resolveRoomAlias(room)

	} else { // This will run if the sync completed with failure
		console.log("client.once('sync'): state is not prepared. State:");
		console.log(state);
		process.exit(1);
	}
});

//client.on

async function main() {
	await sequelizeDatabase.sync()
	.then(() => {
	});
	// await client.initCrypto(); // Starting the encryption layer. Must come before starting the client
	client.startClient({ initialSyncLimit: 10 });
	// populateDatabases(config.initialData, ['rooms']);
}

main();



	// --------------- Message  ---------------
	// Send a message when the bot comes online
	// const content = {
		// body: `${botPrefix} Online!`,
		// msgtype: "m.text",
	// };
	// client.sendEvent(testroomID, "m.room.message", content, '', (err, res) => {
		// console.log(err);
	// });
